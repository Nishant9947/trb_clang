#sudo apt-get update
#sudo apt-get upgrade
#sudo apt-get dist-upgrade
#sudo apt-get install update-manager-core
#sudo do-release-upgrade

export PATH=/workspace/clang/bin:$PATH
export LD_LIBRARY_PATH=/workspace/clang/lib:$LD_LIBRARY_PATH
#./build-llvm.py \
#        --clang-vendor "TheRagingBeast" \
#        --projects "clang;lld;polly" \
#        --targets "ARM;AArch64;X86" \
#        --shallow-clone \
#        --incremental \
#        --defines "LLVM_PARALLEL_COMPILE_JOBS=16//LLVM_PARALLEL_LINK_JOBS=2//CMAKE_C_FLAGS=-O3 -mllvm -polly -mllvm -polly-vectorizer=stripmine -mllvm -polly-run-inliner -mllvm -polly-run-dce//CMAKE_CXX_FLAGS=-O3 -mllvm -polly -mllvm -polly-vectorizer=stripmine -mllvm -polly-run-inliner -mllvm -polly-run-dce//LLVM_USE_LINKER=lld" \
#        --lto full \
#        --build-type "Release" \
#        --no-update

#./build-binutils.py --targets arm aarch64 x86_64 --binutils-folder binutils-gdb

git commit -sm "TheRagingBeast: Bump to 15-12-2022 [DD/MM/YYYY] build
LLVM commit: https://github.com/llvm/llvm-project/commit/4a5d0d8704aa2860c0459a63adfdd415a451c4f1
Clang Version: 16.0.0
Binutils version: 2.39
Projects enabled: clang;lld;polly
LTO: full
Targets: ARM;AArch64;X86"